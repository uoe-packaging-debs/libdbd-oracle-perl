libdbd-oracle-perl (1.80-3) uoe; urgency=medium

  [ Alex Muntada ]
  * Fix lintian checks:
    + spelling errors
    + hardening-no-bindnow

  [ Magnus Hagdorn ]
  * build on School of GeoSciences ubuntu

 -- Magnus Hagdorn <magnus.hagdorn@ed.ac.uk>  Tue, 05 Oct 2021 09:42:26 +0100

libdbd-oracle-perl (1.80-2) unstable; urgency=medium

  [ gregor herrmann ]
  * debian/control: update Build-Depends for cross builds.
  * debian/watch: use uscan version 4.
  * Replace '--with perl_dbi' in debian/rules with a build dependency on
    'dh-sequence-perl-dbi'.
  * Update lintian overrides for renamed tags.

  [ Xavier Guimard ]
  * Bump debhelper compatibility level to 13
  * Declare compliance with policy 4.5.0
  * Add "Rules-Requires-Root: no"
  * Add debian/gbp.conf
  * Update lintian overrides
  * Set upstream metadata fields: Bug-Submit.
  * Remove obsolete field Name from debian/upstream/metadata (already
    present in machine-readable debian/copyright).

  [ Alex Muntada ]
  * Hint ld that libclntsh deps are needed.
  * Remove groff-message lintian override.

 -- Alex Muntada <alexm@debian.org>  Thu, 12 Nov 2020 23:26:45 +0000

libdbd-oracle-perl (1.80-1) unstable; urgency=medium

  * README.Debian: make sure it's installed (Closes: #932257)
  * Import upstream version 1.80

 -- Alex Muntada <alexm@debian.org>  Sun, 06 Oct 2019 17:24:02 +0000

libdbd-oracle-perl (1.76-1) unstable; urgency=medium

  * Import upstream version 1.76

 -- Alex Muntada <alexm@debian.org>  Sun, 06 Jan 2019 01:27:49 +0100

libdbd-oracle-perl (1.74-6) unstable; urgency=medium

  * Add missing libs so LD_LIBRARY_PATH isn't needed

 -- Alex Muntada <alexm@debian.org>  Sat, 05 Jan 2019 16:35:20 +0000

libdbd-oracle-perl (1.74-5) unstable; urgency=medium

  [ Damyan Ivanov ]
  * change Priority from 'extra' to 'optional'

  [ Salvatore Bonaccorso ]
  * Update Vcs-* headers for switch to salsa.debian.org

  [ Alex Muntada ]
  * Update OCI download URL
  * Use short URL to debian/README
  * debian/README.source: reword to avoid repetition
  * Declare compliance with Debian Policy 4.2.1
  * Bump debhelper compatibility level to 10
  * Add debian/upstream/metadata
  * Update lintian overrides
  * Add explanation for being in contrib

 -- Alex Muntada <alexm@debian.org>  Sat, 03 Nov 2018 23:44:53 +0000

libdbd-oracle-perl (1.74-4) unstable; urgency=medium

  * Rebuild with perl 5.26.
  * debian/control:
    + Remove Julián Moreno Patiño from Uploaders.
    + Update Alex Muntada e-mail in Uploaders.
    + Fix spelling error in extended description.
  * debian/*.lintian-overrides:
    + Update details for long line in man page.
    + Add override for long URL in extended description.

 -- Alex Muntada <alexm@debian.org>  Tue, 01 Aug 2017 17:48:38 +0000

libdbd-oracle-perl (1.74-3) unstable; urgency=medium

  [ gregor herrmann ]
  * debian/copyright: change Copyright-Format 1.0 URL to HTTPS.

  [ Alex Muntada ]
  * debian/README*: improve documentation.
    Thanks to Christoph Biedl (Closes: #833119)
  * debian/control: Rewrite full description (#833119)
    Thanks to Christoph Biedl
  * debian/copyright: Add myself to debian/*

 -- Alex Muntada <alexm@alexm.org>  Mon, 26 Sep 2016 22:29:25 +0000

libdbd-oracle-perl (1.74-2) unstable; urgency=medium

  * Set Standards-Version to 3.9.7.

 -- Alex Muntada <alexm@alexm.org>  Mon, 22 Feb 2016 23:28:14 +0000

libdbd-oracle-perl (1.74-1) unstable; urgency=medium

  [ gregor herrmann ]
  * Drop bzip2 compression for source package, xz is the default in dpkg
    since 1.17.0.
  * Strip trailing slash from metacpan URLs.

  [ Salvatore Bonaccorso ]
  * Update Vcs-Browser URL to cgit web frontend

  [ Axel Beckert ]
  * Rename debian/README.maintainer to debian/README.source.
  * Mention special upload requirements for DMs in README.source.

  [ gregor herrmann ]
  * Update path in lintian override.

  [ Alex Muntada ]
  * New upstream release.
  * Remove patches (merged upstream).
  * Set Standards-Version to 3.9.6.
  * Fix reference to examples/ora_explain.pl.
  * Update lintian overrides.

  [ Salvatore Bonaccorso ]
  * debian/control: Use HTTPS transport protocol for Vcs-Git URI

  [ Alex Muntada ]
  * Add myself to Uploaders.
  * Now builds on perl 5.22.1 (Closes: #759324).

 -- Alex Muntada <alexm@alexm.org>  Sun, 07 Feb 2016 12:13:53 +0000

libdbd-oracle-perl (1.66-1) unstable; urgency=low

  * Team upload.

  [ gregor herrmann ]
  * debian/control: update {versioned,alternative} (build) dependencies.
  * Fix package name in debian/README. Thanks to Martin Michlmayr for the
    bug report. (Closes: #684970)

  [ Salvatore Bonaccorso ]
  * Change Vcs-Git to canonical URI (git://anonscm.debian.org)
  * Change search.cpan.org based URIs to metacpan.org based URIs

  [ gregor herrmann ]
  * New upstream release.
  * Update debian copyright: remove information about removed file, update
    years of packaging copyright.
  * Declare compliance with Debian Policy 3.9.4.
  * debian/rules: drop override_dh_auto_install.
    Permissions are correct now.
  * Build against Oracle Instant Client 12.1.
    Update debian/{rules,control} and debian/README*.
  * Add a patch to fix some spelling mistakes.
  * Add a patch to fix POD syntax errors.
  * Add a lintian override for a long line in a manpage.

 -- gregor herrmann <gregoa@debian.org>  Fri, 30 Aug 2013 15:41:51 +0200

libdbd-oracle-perl (1.44-1) unstable; urgency=low

  * Team upload.
  * New upstream release.
  * Update Oracle library version de debian/rules.
  * Use debhelper 9.20120312 to get all hardening flags.

 -- gregor herrmann <gregoa@debian.org>  Fri, 15 Jun 2012 01:42:55 +0200

libdbd-oracle-perl (1.42-1) unstable; urgency=low

  [ Julián Moreno Patiño ]
  * New upstream release.
  * Bump Standards-Version to 3.9.3.
    + Update DEP5 to copyright-format 1.0.
  * Sort Depends and Build-Depends.
  * Bump debhelper version to 9.
    + Switch compat level to 9 to use
      hardening flags.
  * Remove patches.
    + Merge with upstream.

  [ gregor herrmann ]
  * debian/rules: update the fix for the hashbang in the examples.

 -- Julián Moreno Patiño <darkjunix@gmail.com>  Sun, 25 Mar 2012 02:04:46 -0500

libdbd-oracle-perl (1.38-1) unstable; urgency=low

  * New upstream release. (Closes: #651211)
  * Remove debian/libdbd-oracle-perl.docs.
    + Upstream removes the documentation files.
  * Add alternative Dependency to Depends. (Closes: #654916)
    + oracle-instantclient11.2-basic |
      oracle-instantclient11.2-basiclite
  * Extend debian copyright holders years.
  * Update 00_Fix_Spelling_Errors.diff patch to fix
    spelling errors.
  * Add fix-manpage-has-bad-whatis-entry.diff to
    fix manpage.
  * Add sed call in override_dh_installexamples to
    fix wrong path for interpreter.
  * Add optional use of Basic Lite client in
    debian/README.maintainer.

  [ Gregor Herrmann ]
  * Override substvars in debian/rules to get our
    alternative dependency.

 -- Julián Moreno Patiño <darkjunix@gmail.com>  Sat, 14 Jan 2012 18:12:22 -0500

libdbd-oracle-perl (1.34-1) unstable; urgency=low

  * New upstream release.

 -- Julián Moreno Patiño <darkjunix@gmail.com>  Mon, 07 Nov 2011 01:00:12 -0500

libdbd-oracle-perl (1.32-1) unstable; urgency=low

  * New upstream release.
  * Fix path in debian/libdbd-oracle-perl.examples.
  * Update spelling patch and forward to upstream.

 -- Julián Moreno Patiño <darkjunix@gmail.com>  Sat, 29 Oct 2011 14:34:04 -0500

libdbd-oracle-perl (1.30-1) unstable; urgency=low

  [ gregor herrmann ]
  * Fix typo (rpm vs. deb) in debian/README.

  [ Ansgar Burchardt ]
  * debian/control: Convert Vcs-* fields to Git.

  [ Salvatore Bonaccorso ]
  * debian/copyright: Replace DEP5 Format-Specification URL from
    svn.debian.org to anonscm.debian.org URL.

  [Julián Moreno Patiño]
  * New upstream release.
  * Remove unused lintian override.
  * Fix typo in changelog.
  * Update spelling patch.
  * Bump build-dependency on perl to >= 5.10.1.
  * Update copyright holders/years.
    + Update to DEP5 rev174.

 -- Julián Moreno Patiño <darkjunix@gmail.com>  Wed, 14 Sep 2011 21:43:36 -0500

libdbd-oracle-perl (1.28-1) unstable; urgency=low

  * Team upload.

  [ Ansgar Burchardt ]
  * Use perl_dbi addon for dh.
    + Bump build-dependency on libdbi-perl to >= 1.612.

  [ Julián Moreno Patiño ]
  * New upstream release 1.26.
  * Bumped Standards-Version to 3.9.1 (no changes).
  * Updated license details.

  [ gregor herrmann ]
  * New upstream release 1.28.
  * Set Standards-Version to 3.9.2 (no changes).
  * Bump debhelper compatibility level to 8.
  * Remove version from libdbi-perl runtime dependency, already satisfied in
    oldstable.
  * Pass Oracle client version (11.2.0.2.0) to dh_auto_configure.
  * Refresh lintian override.
  * Extend spelling patch.

 -- gregor herrmann <gregoa@debian.org>  Thu, 05 May 2011 16:38:34 +0200

libdbd-oracle-perl (1.24b-1) unstable; urgency=low

  [ Julián Moreno Patiño ]
  * New upstream release.
    + Add libaio1 to Depends. (Closes: #483141)
    + New version works with new instant client versions. (Closes: #535260)
  * New maintainer. Closes: #548322
  * debian/control:
    + Set Uploaders field to Julián Moreno Patiño.
    + Adjusted B-D to newer oracle instant clients versions > 11.1
  * debian/patches: Added, fixing spelling mistakes.
  * debian/copyright: Updated ora_explain.PL license details.
  * Bumped Standards-Version to 3.8.4 (no changes).

  [ Damyan Ivanov ]
  * convert debian/rules to debhelper 7 tiny with overrides
  * debian/control: Added: Vcs-Svn field (source stanza); Vcs-Browser
    field (source stanza); ${misc:Depends} to Depends: field. Changed:
    Maintainer set to Debian Perl Group <pkg-perl-
    maintainers@lists.alioth.debian.org> (was: Peter Eisentraut
    <petere@debian.org>); Peter Eisentraut <petere@debian.org> moved to
    Uploaders.
  * debian/watch: use dist-based URL.
    + drop uupdate, we use svn-buildpackage
  * long description: remove version from the suggested client package. the
    module now works with the recent clients
  * remove note in README.maintainer about inability to build with recent
    clients

  [ Martín Ferrari ]
  * debian/watch: upstream started putting letters in their version numbers.

  [ Ansgar Burchardt ]
  * debian/control: Make build-dep on perl unversioned.
  * Add dependency on perl-dbdapi-* (see #577209).
    + Needs build-dep on libdbi-perl (>= 1.610.90+is+1.609-1~).

  [ gregor herrmann ]
  * debian/rules:
    - don't exclude example README, fix its permissions
    - don't remove /usr/share/perl5/ anymore, EUMM is fixed
  * debian/control: mention module name in long description.
  * lintian-override: make more generic, the path to the lib includes the
    version; add comment.
  * debian/patches: refresh and update headers.
  * debian/copyright: update formatting and license details.

 -- Julián Moreno Patiño <darkjunix@gmail.com>  Sun, 13 Jun 2010 15:57:14 -0500

libdbd-oracle-perl (1.21-2) unstable; urgency=low

  * Only remove /usr/share/perl5/ if it exists; apparently, newer Perl
    versions don't create it
  * Rebuilt for Perl 5.10 (closes: #480462)
  * Added lintian override for rpath issue
  * Updated to debhelper level 6

 -- Peter Eisentraut <petere@debian.org>  Wed, 14 May 2008 17:15:45 +0200

libdbd-oracle-perl (1.21-1) unstable; urgency=low

  * New upstream release

 -- Peter Eisentraut <petere@debian.org>  Tue, 15 Apr 2008 09:19:30 +0200

libdbd-oracle-perl (1.20-2) unstable; urgency=low

  * Added support for amd64 installations of Oracle and made build rules more
    independent of the Oracle version
  * Removed apparently unnecessary build dependency
    oracle-instantclient-sqlplus

 -- Peter Eisentraut <petere@debian.org>  Mon, 11 Feb 2008 11:38:20 +0100

libdbd-oracle-perl (1.20-1) unstable; urgency=low

  * New upstream release
  * Updated DBI dependencies
  * Updated standards version
  * Added installation instructions in package description
  * Added more examples and documentation to installation
  * Added perl-tk to Suggests for ora_explain

 -- Peter Eisentraut <petere@debian.org>  Mon, 21 Jan 2008 11:32:00 +0100

libdbd-oracle-perl (1.19-1) unstable; urgency=low

  * Initial release

 -- Peter Eisentraut <petere@debian.org>  Wed, 24 Oct 2007 16:47:28 +0200
